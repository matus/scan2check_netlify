const Airtable = require('airtable');

const BASE_ID = process.env.AIRTABLE_BASE_ID;
const API_KEY = process.env.AIRTABLE_API_KEY;

const base = new Airtable({ apiKey: API_KEY }).base(BASE_ID);

const validateInput = (data) => {
  if (!data.scannedValue) {
    throw new Error('missing the value \'scannedValue\'');
  }
  if (!data.deviceId) {
    throw new Error('missing the value \'deviceId\'');
  }
};

const saveData = async (data) => {
  return new Promise((resolve, reject) => {
    base('ScannedCodes')
      .create([
          {
            'fields': {
              'deviceId': data.deviceId,
              'scannedValue': data.scannedValue,
              'formValues': JSON.stringify(data.formValues || {}),
            }
          }
        ],
        function (err, records) {
          if (err) {
            reject(err.message);
          } else {
            resolve('ok');
          }
        }
      );
  });
};

const handler = async (event) => {
  try {
    const data = JSON.parse(event.body);
    validateInput(data);

    await saveData(data);

    return {
      statusCode: 200,
      body: JSON.stringify({
        status: 'ok',
        title: 'Success',
        message: 'Entry was stored'
      }),
    };
  } catch (error) {
    return {
      statusCode: 500,
      body: JSON.stringify({
        title: 'Failed',
        message: error.message
      })
    };
  }
};

module.exports = { handler };
